package aoc.model.entity.child;

import java.util.List;
import aoc.utilities.Vector;
import aoc.model.WorldConstants;
import aoc.model.entity.EntityInterface;
import aoc.model.entity.EntityWithLife;
import aoc.model.entity.child.ChildFactory.IsHitStrategy;
import aoc.model.entity.slipper.SlipperInterface;
import aoc.model.level.Level;

/**
 * Defines the base implementation for all children.
 */
public class Child extends EntityWithLife implements ChildInterface {

    /**
     * The Level which will provide the entity list.
     */
    private final Level level;

    /**
     * The strategy that will be applied when the isHit method will be called.
     */
    private final IsHitStrategy hitStrategy;

    /**
     * The constructor for the child.
     * @param position
     *          The initial position for the entity.
     * @param type
     *          The Children type which provides the initial values to the child.
     * @param level
     *          The Level which will provide the entity list.
     */
    public Child(final Vector position, final Children type, final Level level) {
        super(position, new Vector(type.getXSpeed(), type.getYSpeed()), type.name(), type.getLife());
        this.level = level;
        this.hitStrategy = type.getIsHit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<EntityInterface> isHit(final List<EntityInterface> entitiesList) {
        return this.hitStrategy.hit(this.hitterListChecker(entitiesList), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
        super.update();
        // check boundaries
        if (this.getPosition().getY() < WorldConstants.ROW_CENTERS.get(0) 
                || this.getPosition().getY() > WorldConstants.ROW_CENTERS.get(WorldConstants.WORLD_HEIGHT - 1)) {
            this.setSpeed(new Vector(this.getSpeed().getX(), 0));
        }
        final List<EntityInterface> collider = this.isHit(level.getEntityList());
        collider.forEach(x -> this.damaged(((SlipperInterface) x).hit()));
    }

}
