package aoc.view.menu.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

/**
 * The controller class for the highscores scene.
 * 
 */
public class HighscoresController extends BasicController implements Initializable {

	private static final ObservableList<String> list = FXCollections.observableArrayList("30","60");

	@FXML
	ListView<String> arcade;

	@Override
	public void initialize(final URL location, final ResourceBundle resources) {
		arcade.setItems(list);
	}
}
