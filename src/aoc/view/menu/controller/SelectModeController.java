package aoc.view.menu.controller;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 * The controller class for the SelectMode scene.
 *
 */
public class SelectModeController extends BasicController{

        /**
	 * Set the SelectStage scene.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void playStory(final ActionEvent event) throws IOException {
		sf.setScene("SelectStage.fxml");
		this.showScene(event);
	}
	
	/**
	 * Start game in Arcade Mode.
	 * 
	 * @param event
	 */
	@FXML
	private void playArcade(final ActionEvent event) {
	
	}
}
