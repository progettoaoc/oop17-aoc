package aoc.view.gameview;

import java.util.List;
import aoc.utilities.Input;
import aoc.utilities.Vector;
import javafx.scene.image.Image;

/**
 * The interface that has the method to get in contact Controller and View.
 *
 */
public interface GameSceneRender {

	/**
	 * It initializes and draw the game scene.
	 */
	void init();
	
	/**
	 * Update the view.
	 * @param image
	 *          the character's sprite
	 * @param position
	 *          the character's position in the canvas
	 */
	void draw(Image image, Vector position);
	
	/**
	 * It collects all the inputs to send to the controller.
	 * 
	 * @return List<Input>
	 *           the list where the user's inputs are collected
	 */
	List<Input> getInput();
	
	/**
	 * Method that communicate the player his victory with an alert.
	 */
	void win();
	
	/**
	 * Method that communicate the player his loss with an alert. 
	 */
	void lost();
	
	/**
	 * Method that allows to set on pause the game and to resume it or go back
	 * to the Main Menu.
	 */
	void pause();
	
	/**
	 * Sets the label present in the game view.
	 * @param index
	 *          the number of the level 
	 */
	void setLabel(int index);
}
